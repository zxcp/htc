<?php /** Created by Anton on 06.04.2019. */

namespace TestBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use TestBundle\Classes\Config;

class SettingsController extends Controller
{
    public function editAction(Request $request)
    {
        $form = $this->createForm('TestBundle\Form\SettingsType');
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            Config::set('monthLimit', round(abs((float) $request->request->get('monthLimit')), 2));
            Config::set('adaptiveLimit', (int) $request->request->get('adaptiveLimit'));
            Config::save();

            return $this->redirectToRoute('post_index');
        }

        return $this->render('settings/edit.html.twig', array(
            'config' => Config::get(),
            'form' => $form->createView(),
        ));
    }
}