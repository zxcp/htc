<?php

namespace TestBundle\Controller;

use TestBundle\Classes\Categories;
use TestBundle\Classes\Costs;
use TestBundle\Classes\MonthLimits;
use TestBundle\Classes\Months;
use TestBundle\Entity\Post;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Twig\Error\Error;

/**
 * Post controller.
 */
class PostController extends Controller
{
    /** Lists all post entities. */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $costs = $em->getRepository('TestBundle:Post')->findAll();

        //Преобразовываем ID категории в имя
        foreach ($costs as $cost) {
            $cost->setCategory($em->getRepository('TestBundle:Category')->find($cost->getCategory())->getName());
        }

        return $this->render('post/index.html.twig', array(
            'costs' => $costs,
        ));
    }

    /**
     * Creates a new post entity.
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function newAction(Request $request)
    {
        $post = new Post();
        $form = $this->createForm('TestBundle\Form\PostType', $post);
        $form->handleRequest($request);
        $error = null;

        if ($form->isSubmitted() && $form->isValid()) {
            $ml = new MonthLimits($this->getDoctrine());
            $overrun = $ml->addSum(
                $post->getDate()->format('Y'),
                $post->getDate()->format('m'),
                $post->getDate()->format('d'),
                $post->getSum()
            );

            if ($overrun) {
                if ($overrun < 0) {
                    $error = new Error('Лимит превышен. Для добавления расхода увеличте предел на ' . -$overrun . 'руб.');
                    return $this->render('post/new.html.twig', array(
                        'post' => $post,
                        'form' => $form->createView(),
                        'error' => $error
                    ));
                } else {
                    echo '<script type="application/javascript">alert("Лимит превышен на ' . $overrun . 'руб.")</script>';
                }
            }
            $post = $this->changeCategoryNameToId($post);

            $em = $this->getDoctrine()->getManager();
            $em->persist($post);
            $em->flush();

            return $this->redirectToRoute('post_show', [
                'id' => $post->getId()
            ]);
        }

        return $this->render('post/new.html.twig', array(
            'post' => $post,
            'form' => $form->createView(),
            'error' => $error
        ));
    }

    /** Finds and displays a post entity. */
    public function showAction(Post $post)
    {
        $deleteForm = $this->createDeleteForm($post);

        $em = $this->getDoctrine()->getManager();
        $post->setCategory($em->getRepository('TestBundle:Category')->find($post->getCategory())->getName());

        return $this->render('post/show.html.twig', array(
            'post' => $post,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /** Displays a form to edit an existing post entity. */
    public function editAction(Request $request, Post $post)
    {
        $deleteForm = $this->createDeleteForm($post);
        $editForm = $this->createForm('TestBundle\Form\PostType', $post);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            //Для сохранения в БД меняем категорию с Category на её ID
            $post = $this->changeCategoryNameToId($post);

            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('post_index', array('id' => $post->getId()));
        }

        return $this->render('post/edit.html.twig', array(
            'post' => $post,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /** Deletes a post entity. */
    public function deleteAction(Request $request, Post $post)
    {
        $form = $this->createDeleteForm($post);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($post);
            $em->flush();
            (new MonthLimits($this->getDoctrine()))->clearTable();
        }

        return $this->redirectToRoute('post_index');
    }

    /** Сводный отчёт по годам и месяцам */
    public function reportAction()
    {
        $ml = new Costs($this->getDoctrine());

        return $this->render('post/report.html.twig', [
            'costs' => $ml->getCosts(),
            'months' => Months::getMonthsArray()
        ]);
    }

    /** Детальный отчёт за месяц */
    public function detailAction($year, $month)
    {
        $em = $this->getDoctrine();

        $costs = new Costs($em);

        $costs = $costs->format($costs->getForDate($year, $month));

        $categoriesArr = Categories::getCategoriesArray($em);

        //Подсчёт сумм по категориям
        $sumByCategory = [];
        foreach ($categoriesArr as $category) $sumByCategory[$category] = 0;
        foreach ($costs as $cost)
            foreach ($cost as $categoryName => $data) {
                 $sumByCategory[$categoryName] += $data['sum'];
            }

        return $this->render('post/detail.html.twig', [
            'costs' => Costs::calcTotalSum($costs),
            'categories' => $categoriesArr,
            'sumByCategory' => $sumByCategory
        ]);
    }

    /**
     * Creates a form to delete a post entity.
     *
     * @param Post $post The post entity
     *
     * @return \Symfony\Component\Form\FormInterface
     */
    private function createDeleteForm(Post $post)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('post_delete', array('id' => $post->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }

    /** Меняет имя категории на её ID */
    private function changeCategoryNameToId(Post $post)
    {
        return $post->setCategory($post->getCategory()->getId());
    }
}
