<?php /** Created by Anton on 04.04.2019. */

namespace TestBundle\Classes;

class Config
{
    private static $data = null;
    const CONFIG_FILE = 'limit.ini';

    public static function get($key = null)
    {
        $data = self::getData();
        if (!$key) return self::$data;

        if (!empty($data[$key])) return $data[$key];

        return [];
    }

    public static function set($key, $val)
    {
        $data = self::getData();
        $data[$key] = $val;
        self::$data = $data;
    }

    public static function save()
    {
        $data = self::getData();
        $path = __DIR__ . '/../Config/' . self::CONFIG_FILE;
        $res = '';
        foreach ($data as $key => $val) {
            $res .= $key . '=' . $val . "\n";
        }
        file_put_contents($path, $res);
        self::$data = null;//При следующем обращении к классу данные обновятся
    }

    private function getData()
    {
        if (self::$data != null) return self::$data;

        $path = __DIR__ . '/../Config/' . self::CONFIG_FILE;
        if (!file_exists($path)) {
            self::$data = [];
            return self::$data;
        }

        self::$data = parse_ini_file($path);
        return self::$data;
    }
}
