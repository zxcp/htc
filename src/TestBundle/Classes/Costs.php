<?php
/**
 * Created by PhpStorm.
 * User: Fantom
 * Date: 10.04.2019
 */

namespace TestBundle\Classes;

use Doctrine\Common\Persistence\ManagerRegistry;

/** Класс для работы с расходами */

class Costs
{
    private $costs = [];
    private $mr;
    private static $instance = null;

    public function __construct(ManagerRegistry $doctrine) {
        $this->mr = $doctrine;
        $this->setCosts();
    }

    public static function getInstance(ManagerRegistry $doctrine)
    {
        if (self::$instance == null) {
            self::$instance = new Costs($doctrine);
        }

        return self::$instance;
    }

    public function addSum($year, $month, $day, $sum)
    {
        if (isset($this->costs[$year][$month])) {
            $this->costs[$year][$month] += $sum;
        } else {
            $this->costs[$year][$month] = $sum;
        }
        $date = new \DateTime();
        $date->setDate($year, $month, $day);
    }

    /** Возвращает массив расходов за каждый месяц [<year> => [<month> => <cost>]] */
    public function getCosts()
    {
        return $this->costs;
    }

    /** @return double сумма расходов по дате */
    public function getCostBy($year = null, $month = null)
    {
        if ($year && $month && isset($this->costs[$year][$month])) return $this->costs[$year][$month];

        return 0;
    }

    /** @return array все расходы за месяц */
    public function getForDate($year, $month)
    {
        $dateFrom = new \DateTime();
        $dateFrom->setDate($year, $month, 1);
        $dateFrom->setTime(0, 0);
        $dateTo = new \DateTime();
        $dateTo->setDate($year, $month, 31);
        $dateTo->setTime(23, 59, 59);

        $qb = $this->mr->getManager()->createQueryBuilder()
            ->select('c')
            ->from('TestBundle:Post', 'c')
            ->where('c.date BETWEEN :from AND :to')
            ->setParameter('from', $dateFrom)
            ->setParameter('to', $dateTo);

        $costs = $qb->getQuery()->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);

        return $costs;
    }

    public function getCostsForDate($year, $month)
    {
        $sum = 0;
        $costs = $this->getForDate($year, $month);
        foreach ($costs as $cost) {
            $sum += $cost['sum'];
        }

        return $sum;
    }

    /** @return array [day => [categoryName => cost]] */
    public function format($costs)
    {
        $costsArr = [];
        foreach ($costs as $cost) {
            $day = $cost['date']->format('d');
            $category = $this->mr->getRepository('TestBundle:Category')->find($cost['category']);
            $categoryName = $category->getName();
            if (isset($costsArr[$day][$categoryName])) {
                $costsArr[$day][$categoryName]['sum'] += $cost['sum'];
            } else {
                $costsArr[$day][$categoryName] = $cost;
            }
        }

        return $costsArr;
    }

    /**
     * Подсчёт итоговых сумм за каждый день
     * @return array
     */
    public static function calcTotalSum(array $costsArray)
    {
        foreach ($costsArray as $day => $costs) {
            foreach ($costs as $category => $cost) {
                if (isset($costsArray[$day]['total'])) {
                    $costsArray[$day]['total'] += $cost['sum'];
                } else {
                    $costsArray[$day]['total'] = $cost['sum'];
                }
            }
        }

        return $costsArray;
    }

    /** Заполняет $this->costs массивом с расходами за каждый месяц. */
    private function setCosts()
    {
        $this->costs = [];
        $costs = $this->mr->getRepository('TestBundle:Post')->findAll();
        foreach ($costs as $cost) {
            $year = $cost->getDate()->format('Y');
            $month = $cost->getDate()->format('m');
            if (isset($this->costs[$year][$month])) {
                $this->costs[$year][$month] += $cost->getSum();
            } else {
                $this->costs[$year][$month] = $cost->getSum();
            }
        }
        //Упорядочиваем месяцы
        foreach ($this->costs as $year => $month) {
            ksort($this->costs[$year]);
        }
    }
}
