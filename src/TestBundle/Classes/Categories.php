<?php
/**
 * Created by PhpStorm.
 * User: Fantom
 * Date: 10.04.2019
 */

namespace TestBundle\Classes;

use Doctrine\Common\Persistence\ManagerRegistry;

class Categories
{
    /** @return array [idCategory => categoryName] */
    public static function getCategoriesArray(ManagerRegistry $doctrine)
    {
        $categories = $doctrine->getRepository('TestBundle:Category')->findAll();
        $categoriesArr = [];
        foreach ($categories as $category) {
            $categoriesArr[$category->getId()] = $category->getName();
        }

        return $categoriesArr;
    }
}
