<?php
/**
 * Created by PhpStorm.
 * User: Fantom
 * Date: 10.04.2019
 */

namespace TestBundle\Classes;

class Months
{
    /** @return array [<номер месяца> => <назвнание>] */
    public static function getMonthsArray()
    {
        return [
            '01' => 'Январь',
            '02' => 'Февраль',
            '03' => 'Март',
            '04' => 'Апрель',
            '05' => 'Май',
            '06' => 'Июнь',
            '07' => 'Июль',
            '08' => 'Август',
            '09' => 'Сентябрь',
            '10' => 'Октябрь',
            '11' => 'Ноябрь',
            '12' => 'Декабрь',
        ];
    }

    /** @return array ['year' => <год>, 'month' => <месяц>] */
    public static function addMonthToDate($year, $month)
    {
        if ($month == 12) {
            $month = 1;
            $year++;
        } else {
            $month++;
        }
        if ($month < 10) $month = '0' . $month;//Формат месяца XX

        return ['year' => $year, 'month' => $month];
    }
}
