<?php
/**
 * Created by PhpStorm.
 * User: Fantom
 * Date: 08.04.2019
 * Time: 12:17
 */

namespace TestBundle\Classes;

use Doctrine\Common\Persistence\ManagerRegistry;
use TestBundle\Entity\MonthLimit;

/** Класс для работы с пределами расходов */

class MonthLimits
{
    private $limits = [];
    private $mr;

    public function __construct(ManagerRegistry $doctrine)
    {
        $this->mr = $doctrine;
        if (Config::get('adaptiveLimit')) $this->setLimits();
    }

    public function getLimits()
    {
        return $this->limits;
    }

    /**
     * Добавляет сумму к расходам месяца
     * @return int сумма превышения лимита. Отрицательна при превышении фиксированного лимита.
     */
    public function addSum($year, $month, $day, $sum)
    {
        $costs = Costs::getInstance($this->mr);
        if (Config::get('adaptiveLimit')) {
            $costs->addSum($year, $month, $day, $sum);

            return $this->addToLimit($year, $month, $sum);

        } else {
            $monthCost = $costs->getCostBy($year, $month) + $sum;
            if ($monthCost > Config::get('monthLimit')) {
                return -($monthCost - Config::get('monthLimit'));
            } else {
                $costs->addSum($year, $month, $day, $sum);
//                $this->addToLimit($year, $month, $sum);
            }
        }

        return 0;
    }

    /**
     * Добавляет расход, с переносом превышения предела на следующие месяцы
     * @return double превышение лимита
     */
    private function addToLimit($year, $month, $sum)
    {
        if (isset($this->limits[$year][$month])) {
            $this->limits[$year][$month] += $sum;
        } else {
            $this->limits[$year][$month] = $sum;
        }

        if ($this->limits[$year][$month] > Config::get('monthLimit')) {
            $sum = Costs::getInstance($this->mr)->getCostsForDate($year, $month);
            $sum += $this->limits[$year][$month];

            $overrun = $sum - Config::get('monthLimit');
            $this->limits[$year][$month] = Config::get('monthLimit');
            $this->flushOne($year, $month, Config::get('monthLimit'));
            $date = Months::addMonthToDate($year, $month);
            $this->addToLimit($date['year'], $date['month'], $overrun);

            return $overrun;
        }
        $this->flushOne($year, $month, $this->limits[$year][$month]);

        return 0;
    }

    private function flushOne($year, $month, $sum)
    {
        $em = $this->mr->getManager();
        $ml = $em->getRepository('TestBundle:MonthLimit')->findOneBy(['date' => $year . '-' . $month]);
        if ($ml) {
            $ml->setSum($sum);
        } else {
            $ml = new MonthLimit();
            $ml ->setDate($year . '-' . $month)
                ->setSum(round($sum, 2));//Откуда-то погрешность вычислений взялась.
            $em->persist($ml);
        }
        $em->flush();
    }

    /** Сохраняет данные в таблицу month_limit */
    public function flush()
    {
        $em = $this->mr->getManager();
        foreach ($this->prepareToSave() as $date => $limit) {
            $ml = new MonthLimit();
            $ml ->setDate($date)
                ->setSum($limit);
            $em->persist($ml);
        }

        $em->flush();
    }

    /** Возвращает массив для сохранения в таблицу month_limit [<Y-m> => <limit>] */
    public function prepareToSave()
    {
        $limits = [];
        foreach ($this->limits as $year => $monthLimits) {
            foreach ($monthLimits as $month => $limit) {
                $limits[$year . '-' . $month] = $limit;
            }
        }
        return $limits;
    }

    public function clearTable()
    {
        $monthLimits = $this->mr->getRepository('TestBundle:MonthLimit')->findAll();
        $em = $this->mr->getManager();
        foreach ($monthLimits as $ml) {
            $em->remove($ml);
        }
        $em->flush();

        return $this;
    }

    private function setLimits()
    {
        $limits = $this->mr->getRepository('TestBundle:MonthLimit')->findAll();
        if (empty($limits)) {
            foreach (Costs::getInstance($this->mr)->getCosts() as $year => $monthCosts) {
                foreach ($monthCosts as $month => $cost) {
                    $this->addToLimit($year, $month, $cost);
                }
            }
        } else {
            foreach ($limits as $limit) {
                list($year, $month) = explode('-', $limit->getDate());
                $this->limits[$year][$month] = $limit->getSum();
            }
        }
    }
}
