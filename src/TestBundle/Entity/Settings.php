<?php /** Created by Anton on 07.04.2019. */

namespace TestBundle\Entity;

class Settings
{
    private $monthLimit;

    private $adaptiveLimit;

    public function setMonthLimit($monthLimit)
    {
        $this->monthLimit = $monthLimit;

        return $this;
    }

    public function getMonthLimit()
    {
        return $this->monthLimit;
    }

    public function setAdaptiveLimit($adaptiveLimit)
    {
        $this->adaptiveLimit = $adaptiveLimit;

        return $this;
    }

    public function getAdaptiveLimit()
    {
        return $this->adaptiveLimit;
    }
}