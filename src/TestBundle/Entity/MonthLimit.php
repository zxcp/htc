<?php

namespace TestBundle\Entity;

use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\Mapping as ORM;
use TestBundle\Classes\Helper;

/**
 * MonthLimit
 *
 * @ORM\Table(name="month_limit")
 * @ORM\Entity(repositoryClass="TestBundle\Repository\MonthLimitRepository")
 */
class MonthLimit
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="date", type="string", unique=true)
     */
    private $date;

    /**
     * @var float
     *
     * @ORM\Column(name="sum", type="float", nullable=true)
     */
    private $sum;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param string
     *
     * @return MonthLimit
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return string
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set sum
     *
     * @param float $sum
     *
     * @return MonthLimit
     */
    public function setSum($sum)
    {
        $this->sum = $sum;

        return $this;
    }

    /**
     * Get sum
     *
     * @return float
     */
    public function getSum()
    {
        return $this->sum;
    }
}