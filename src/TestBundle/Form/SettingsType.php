<?php /** Created by Anton on 06.04.2019. */

namespace TestBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use TestBundle\Classes\Config;

class SettingsType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('monthLimit', MoneyType::class, [
                'label' => false,
                'currency' => 'RUB',
                'data' => Config::get('monthLimit'),
                'attr' => [
                    'placeholder' => 'Месячный лимит',
                ]
            ])
            ->add('adaptiveLimit', CheckboxType::class, [
                'label' => 'Адаптивный предел',
                'data' => (bool) Config::get('adaptiveLimit'),
                'required' => false
            ]);
    }
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'TestBundle\Entity\Settings'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
    }

}
